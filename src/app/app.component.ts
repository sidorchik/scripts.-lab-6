import { Component, ViewChild, ElementRef, OnInit } from '@angular/core';

@Component({
   selector: 'app-root',
   templateUrl: './app.component.html',
   styleUrls: ['./app.component.css']
})
export class AppComponent {
    index: number = 10;
    sensors = [
        {
            name: "Датчик 1",
            online: this.check()
        },
        {
           name: "Датчик 2",
           online: this.check()
        },
        {
           name: "Датчик 3",
           online: this.check()
        },
        {
           name: "Датчик 4",
           online: this.check()
        },
        {
           name: "Датчик 5",
           online: this.check()
        },
        {
           name: "Датчик 6",
           online: this.check()
        },
        {
           name: "Датчик 7",
           online: this.check()
        },
        {
           name: "Датчик 8",
           online: this.check()
        },
        {
           name: "Датчик 9",
           online: this.check()
        }
    ];

    create() {
        this.sensors.push({
            name: "Датчик " + this.index,
            online: false
        });

        this.index++;
    }

    delete(i) {
        this.sensors.splice(i, 1);
    }

    check() {
        if (Math.random() >= 0.5)
            return true;
        else
            return false;
    }
}
